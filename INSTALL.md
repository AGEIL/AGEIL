* installer python3 et virtualenv
* créer un répertoire Projet et y mettre AGEIL
* dans Projet, faire `virtualenv -p python3 myvenv`
* `source myvenv/Bin/activate` (sous Windows : `myvenv\Scripts\activate`)
* `pip install -r AGEIL/pip-requirements.txt`
* pour lancer AGEIL : `python AGEIL/manage.py runserver localhost:9999`
* à la fin : `deactivate`
* pour avoir la liste des dépendances : `pip freeze`
