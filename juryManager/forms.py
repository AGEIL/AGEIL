from django import forms
from home.models import SemestreAnnualise

class SemestreAnnualiseForm(forms.ModelForm):
	class Meta:
		model = SemestreAnnualise
		fields = '__all__'