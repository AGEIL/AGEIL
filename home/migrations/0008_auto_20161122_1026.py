# -*- coding: utf-8 -*-
# Generated by Django 1.10.1 on 2016-11-22 09:26
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('home', '0007_association_etudiant_semestreannualise_modelesemestre'),
    ]

    operations = [
        migrations.AlterField(
            model_name='association_etudiant_semestreannualise',
            name='modeleSemestre',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='home.ModeleSemestre'),
        ),
    ]
